package com.example.demo.common.application.exception;

public class PONotFoundException extends Exception {
    private static final long serialVersionUID = 1L;

    public PONotFoundException(Long id) {
        super(String.format("PO not found! (Plant id: %d)", id));
    }
}
