package com.example.demo.sales.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, Long> {
    @Query("select po from PurchaseOrder po where LOWER(po.status) like %?1% ")
    List<PurchaseOrder> findAllBYStatus (POStatus status);
}
