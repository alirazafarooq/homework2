package com.example.demo.sales.application.service;



import com.example.demo.sales.domain.PurchaseOrder;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.time.LocalDate;

@Service
public class PurchaseOrderValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return PurchaseOrder.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        PurchaseOrder po = (PurchaseOrder) o;

       if(po.getRentalPeriod().getStartDate() == null)
           errors.rejectValue("startDate", "startDate cannot be null");
        if(po.getRentalPeriod().getEndDate() == null)
            errors.rejectValue("endDate", "endDate cannot be null");
//        if(po.getReservations() == null)
//            errors.rejectValue("reservations", "reservations cannot be null");
        if(po.getStatus() == null)
            errors.rejectValue("status", "status cannot be null");
        if(po.getIssueDate() == null)
            errors.rejectValue("issueDate", "issueDate cannot be null");
//        if(po.getTotal() == null)
//            errors.rejectValue("Total", "Total cannot be null");
        if(po.getPlant() == null)
            errors.rejectValue("Plant", "Plant cannot be null");
        if(po.getRentalPeriod().getStartDate()!= null && po.getRentalPeriod().getEndDate() != null) {
            if (!po.getRentalPeriod().getStartDate().isBefore(po.getRentalPeriod().getEndDate()))
                errors.rejectValue("startDate", "startDate must ocurrs before endDate");
            if (po.getRentalPeriod().getStartDate().isBefore(LocalDate.now()))
                errors.rejectValue("startDate", "startDate must be in the future");
            if (po.getRentalPeriod().getEndDate().isBefore(LocalDate.now()))
                errors.rejectValue("endDate", "endDate must be in the future");
        }
    }
}
